provider "aws" {
  region = var.region
}

terraform {
  backend "s3" {
    bucket = "amid-dev-terraform"
    key    = "terraform.tfstate"
    region = "eu-central-1"
  }
}
